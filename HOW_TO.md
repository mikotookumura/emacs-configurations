# Text Editing Key Bindings
## Cursor Moving
* C-a,e,f,b,n,p: システムデフォルト
* M-f: 1ワード前方移動
* M-b: 1ワード後方移動
* M-m: 行頭移動（インデント含まない）
* C-v: 1画面次に移動
* M-v: 1画面前に移動

## Delete
* C-h: 1文字削除
* C-w: 1単語削除
* M-d: 1単語削除（カーソル上）
* C-k:
    * 行頭: 一行削除（改行含む）
    * 行中: 一行削除

## Paste
* C-y: ペースト
* M-y: キル・リングから選択してペースト

## Region
* C-@: 選択範囲拡張
* C-M-@: 選択範囲拡張
* C->: カーソルがあるシンボルと同一の次のシンボルを選択(multiple-cursor)
* C-<: カーソルがあるシンボルと同一の前のシンボルを選択(multiple-cursor)
* C-*: カーソルがあるシンボルと同一のシンボルを全て選択(multiple-cursor)


# Basic Key Bindings
## System
* C-g: コマンドキャンセル（困ったらコレ）
* C-;: Anyhintg find file（ファイルオープン）
    * バッファ
    * 履歴
    * ファイルリスト（デイリー作成）
* M-x: コマンド（Anything）

## Window
* C-x 2: 縦分割
* C-x 3: 横分割
* C-x 0: 現在のウィンドウ分割を削除
* C-x 1: 現在のウィンドウ分割に統合
* C-x o: 次のウィンドウ分割に移動
* Shift-←↓↑→: ウィンドウ分割移動
* M-s-c: 新規スクリーン作成
* M-s-n: 次スクリーンに移動
* M-s-p: 前スクリーンに移動
* M-s-k: 現在のスクリーンに移動

## Shell
* M-!: シェルコマンド
* C-': eshell
* C-Shift-': eshell（new buffer）
* M-h: 履歴から検索（検索対象をタイプしてから利用）
* M-c: Anythingで補完
* `bmk`
    * 登録: `bmk PATH BOOKMARK_NAME`
        * 例）`bmk . BMK1`: カレントディレクトリをBMK1という名前で登録
    * 移動: `bmk BOOKMARK_NAME`
        * 例）`bmk BMK1`: BMK1という名前で登録されたブックマークに移動


# Markdown mode
* C-c C-c o: review
* リスト中で利用
    * M-<return>: New Item
    * M-←→: インデント


# eww: Text Browser
command: eww



# Rest
