#!/bin/sh


# Install Daily Script

## Variables
FILE="${HOME}/.emacs.d/tmp/make-filelist.sh"
PERIODIC=/etc/periodic/daily/
IAM=`who am i | cut -d\  -f 1`

## Generate a daily running script
echo "#!/bin/sh" > $FILE
echo "" >> $FILE
echo "env HOME=${HOME} ruby ${HOME}/.emacs.d/scripts/make-filelist.rb > ${HOME}/.emacs.d/tmp/all.filelist" >> $FILE

## Change mode & owner
chmod +x $FILE
sudo chown root:wheel $FILE

## Install
sudo mv $FILE $PERIODIC


# Install Setting File
cp "${HOME}/.emacs.d/scripts/.make-filelist.rb" $HOME


# Execute manually this script to make a file-list
ruby $HOME/.emacs.d/scripts/make-filelist.rb > $HOME/.emacs.d/tmp/all.filelist
