#!/bin/sh

# Base Tools
brew  install python3
sudo pip3 install --upgrade setuptools
sudo pip3 install --upgrade pip

# for flymake
pip3 install pyflakes

# for jedi(smart auto-complete for python)
pip3 install virtualenv

# for python-mode because it is on a bazaar repository.
brew install bzr

brew linkapps
