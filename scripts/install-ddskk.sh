#!/bin/sh

VER="15.2"
SOURCE_DIR="ddskk-${VER}"
SOURCE="${SOURCE_DIR}.tar.gz"
URL="http://openlab.ring.gr.jp/skk/maintrunk/${SOURCE}"
WORK_DIR="${HOME}/.emacs.d/tmp/ddskk-build"

# cd Working Directory
mkdir -p $WORK_DIR
cd $WORK_DIR

# Download
curl -O $URL

# Decompose
tar xzf $SOURCE
cd $SOURCE_DIR

# Configure SKK-CFG for Mac
sed -i -e '126,128s/;;//' ./SKK-CFG

# Build & Install
make install EMACS=/Applications/Emacs.app/Contents/MacOS/Emacs
