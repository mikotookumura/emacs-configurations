#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-
# Make almost all filelist in my computer
#
# Features
# * Exclude unused files
# * List directory first
#
# Usage:
# * Print all files in computer
#     ruby make-filelist.rb > all.filelist
# * Print specified directories
#     ruby make-filelist.rb dir1 dir2 ... > partial.filelist
# * Use with cron: add this entry to crontab to make filelist at 1:00 AM.
#     0 1 * * * ruby /path/to/make-filelist.rb > /tmp/all.filelist
#
# Customize:
# You can override customize variables by creating ~/.make-filelist.rb

#
# ==== Customize Variables ====
# Exclude pathnames (version control system directories and so on)
$EXCLUDE_PATH = %w[
. .. backup backup1 backup2 backup-system lost+found tmp temp
.cache w3mtmp proxy-cache trash proc RCS.old
autom4te.cache blib _build .bzr .cdv cover_db CVS _darcs ~.dep ~.dot .git .hg ~.nib .pc ~.plst RCS SCCS _sgbak .svn

Applications Downloads
.heroku .boot2docker .m2 .subversion .ssh git svn heroku eclipse ローカル開発環境 商用データ
.dropbox .Trash Library iTunes .nave .rvm .gem .npm node_modules vendor log
~.chache .localized uploads build lib .fontconfig .local el-get .config snippets images img
.DS_Store
]

# Exclude regexps (backup files, core files, and so on)
$EXCLUDE_REGEXP = Regexp.union(/~$/, /\#.+\#$/, /[._].*\.swp$/, /core\.\d+$/, # from ack-grep
/\.(?:elc|o)$/, /,v$/,
/Microsoft/, /VirtualBox/,
/^\d+$/,
/^\/Applications/, /\.dayone/, /\.node/)

# Set default directories to collect
# $LS_DIRS = ["~", "/"]
$LS_DIRS = ["~"]
# ==== End of Customize Variables =====
