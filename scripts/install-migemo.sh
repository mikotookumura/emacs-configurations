#!/bin/sh

VER=20110227
SOURCE_DIR=cmigemo-default-src
SOURCE="${SOURCE_DIR}-${VER}.zip"
URL="https://cmigemo.googlecode.com/files/${SOURCE}"
WORK_DIR="${HOME}/.emacs.d/tmp/migmo-build"

# cd Working Directory
mkdir -p $WORK_DIR
cd $WORK_DIR

# Install nkf
brew install nkf

# Download
curl -O $URL

# Decompose
unzip $SOURCE
cd $SOURCE_DIR

# Configure
chmod +x configure
./configure

# add: #include <limits.h>
sed -i -e '12i\
#include <limits.h>
' ./src/wordbuf.c

# Build & Install
make osx
make osx-dict
cd dict
make utf-8
cd ..
sudo make osx-install
