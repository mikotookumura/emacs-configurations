#!/bin/sh

WORK_DIR="${HOME}/.emacs.d/tmp/emacs-build"


if [ $# -eq 1 ] ; then
    URL=$1

    # Get File Information Variables From URL
    EMACS_GZ=${URL##*/}
    EMACS_DIR=${EMACS_GZ%.*.*}
    LAST_EXT=${EMACS_GZ##*.}

    # cd Working Directory
    mkdir -p $WORK_DIR
    cd $WORK_DIR

    if `expr $1 : http > /dev/null` ; then
        # if $1 is an URL
        # Download An Emacs Source
        echo $URL
        curl -L -O $URL
    else
        # if $1 is a file path
        cp $1 .
    fi

    # Decomposing
    if [ $LAST_EXT -eq "gz" ] ; then
        tar xvzf $EMACS_GZ
    else
        tar Jxvf $EMACS_GZ
    fi
    cd $EMACS_DIR

    # Install some packages
    brew install autoconf
    brew install automake
    brew install svn
    brew install ctags

    # Build
    ./configure --with-ns --without-x
    make bootstrap
    make install

    # cp to /Applications/
    cp -r nextstep/Emacs.app /Applications/Emacs.app


    echo "\nInstall was completed."
    echo "if you want run Emacs.app from your terminal,"
    echo "write the following alias in .bash_profile, .bashsrc, ..etc."
    echo "  alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'"
else
    echo "Usage1:"
    echo '  ./install-emacs.sh $EMACS_SOURCE_URL'
    echo "Usage2:"
    echo '  ./install-emacs.sh $EMACS_SOURCE_FILE_PATH (The file must be tar.gz or tar.xz.)\n'

    echo "Emacs for Mac OS X can be downloaded from:"
    echo "  http://emacsformacosx.com/builds"
fi
