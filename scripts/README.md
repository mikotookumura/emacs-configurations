# Install Emacs

## Install

    $ ./install-emacs.sh $EMACS_SOURCE_URL

インストール後、Emacsを再起動すると、el-getをインストールして固まる（初回はel-get自身のインストール）  
が、気にせず強制終了し、再度Emacsを起動すると、残りのパッケージを読み込む

### 例

    $ ./install-emacs.sh  http://emacsformacosx.com/emacs-sources/emacs-24.4.tar.xz

Mac版Emacsのソースは、http://emacsformacosx.com/builds にある


## スクリプト解説
Reference: http://fukubaya.blogspot.jp/2012/06/macbookair-w-lion-emacs-241.html

### 作業ディレクトリ作成

	$ mkdir emacs-24.2-build
	$ cd emacs-24.2-build

### ソースダウンロード

	$ curl -O http://ftp.gnu.org/pub/gnu/emacs/emacs-24.2.tar.gz

### 展開

	$ tar xvzf emacs-24.2.tar.gz
	$ cd emacs-24.2

### configure, make, install

	$ ./configure --with-ns --without-x
	$ make bootstrap
	$ make install
	$
	$ cp -r nextstep/Emacs.app /Applications/
	$ sudo chmod 777 ~/.emacs.d

（--with-nsオプションは、Emacs.app内に全リソースを含める）

### エイリアス設定（デフォルトのターミナルから起動するため）
.bash_profile等に、下記を追加するとコマンドで起動できる

	alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'





# Install Optional Libraries

## anything-filelist+
Reference: http://d.hatena.ne.jp/rubikitch/20100915/anything

* パターン検索でファイルを開くことができる
* 補完対象のファイルリストを作成するため、dailyでクローリングするクローラーのプログラムをインストールする
    * /etc/periodic/daily/make-filelist.sh を作成する
    * その他、~/.emacs.d/ 以下にスクリプト及びファイルリストを作成する
* 実行にrubyが必要

### Install

    $ ./install-make-filelist.sh

### ファイルリストカスタマイズ
~/.make-filelist.rb を編集することで、ファイルリストを作成する際のクローリングルールを変更できる  
~/.make-filelist.rb が存在するならば、読み込まれ、デフォルトの設定を上書きする

### 設定
~/.emacs.d/el-get-init-files/init-anything.elに以下のように記述してある

	(require 'anything-startup)
	(setq anything-c-filelist-file-name "~/.emacs.d/tmp/all.filelist")



## migemo
Reference: http://d.hatena.ne.jp/ground256/20111008/1318063872

ローマ字入力で日本語インクリメンタルサーチする（migemo）

### Install

    $ ./install-migemo.sh

### スクリプト解説
* cmigemo開発: http://www.kaoriya.net/software/cmigemo
* cmigemoソース: http://code.google.com/p/cmigemo/downloads/list

上記からcmigemoのソースをDL後、

	$ chmod +x configure
	$ ./configure

src/wordbuf.cにlimits.hのインクルードを追加

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <limits.h>/* この行を追加 */
	#include "wordbuf.h"

下記コマンドでインストール

	$ make osx
	$ make osx-dict
	$ cd dict
	$ make utf-8
	$ cd ..
	$ sudo make osx-install




## ddskk
ddskk

### Install

    $ ./install-migemo.sh


### スクリプト解説

#### ダウンロード
Reference: http://openlab.ring.gr.jp/skk/ddskk-ja.html

    $ curl -O http://openlab.ring.gr.jp/skk/maintrunk/ddskk-15.2.tar.gz
    $ tar xvzf ddskk-15.2.tar.gz
	$ cd ddskk-15.2

#### インストール
Reference: http://fukubaya.blogspot.jp/2011/12/macbookair-w-lion-cocoa-emacs.html

SKK-CFG のコメントを外す．

	;;; Apple Mac OS X における self-contained な Carbon/Cocoa Emacs の設定例:
	(setq SKK_DATADIR "/Applications/Emacs.app/Contents/Resources/etc/skk")
	(setq SKK_INFODIR "/Applications/Emacs.app/Contents/Resources/info")
	(setq SKK_LISPDIR "/Applications/Emacs.app/Contents/Resources/site-lisp/skk")
	(setq SKK_SET_JISYO t)

make&installで完了

	$ make install EMACS=/Applications/Emacs.app/Contents/MacOS/Emacs

Emacs24.3の場合は以下

	$ make install EMACS=/Applications/Emacs.app/Contents/MacOS/Emacs-10.7


### 権限問題

SKKを利用してからEmacsを終了すると、cannot load ~/.skk* というエラーが出る場合があるので、
該当ファイルの権限を修正

	$ chown +rw .skk*
