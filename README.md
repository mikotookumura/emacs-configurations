# Emacsの設定

## 設定ファイル一覧

* エントリーポイント: ~/.emacs.d/init.el
* 共通設定: ~/.emacs.d/elisp/settings.el
* ddskk設定: ~/.emacs.d/elisp/ddskk-settings.el
* パッケージ置き場: ~/.emacs.d/el-get/
* パッケージ設定ファイル置き場: ~/.emacs.d/el-get-init-files/
* スクリプト置き場: ~/.emacs.d/scripts/
* eshellのalias: ~/.emacs.d/eshell/alias


## 章構成

* 1.: 必須
* 2.〜5.: 必要に応じて
* 6.: eshell以外のshellを使う人は推奨


## 諸注意

* eshell 利用者は、必要に応じて下記を編集する必要がある
    * 環境パス: ~/.emacs.d/el-get-init-files/init-shell-switcher.el
    * エイリアス: ~/.emacs.d/eshell/alias
* ddskk が不要な場合は、~/.emacs.d/init.elの`(require 'ddskk-settings)`をコメントアウトする


---

## 1. Cocoa Emacsをインストール

### Clone & Setup

    $ git clone [this project]
    $ mv -r [this project] ~/.emacs.d

プロジェクトをクローンし、~/.emacs.d として設置する

### Install

    $ cd ~/.emacs.d/scripts
    $ ./install-emacs.sh [emacs source url]

インストール後、Emacsを再起動すると、el-getをインストールして固まる（初回はel-get自身のインストール）  
が、気にせず強制終了し、再度Emacsを起動すると、残りのパッケージを読み込む

詳細は、[/scripts/](tree/master/scripts)を参考


---


## 2. パターン検索でファイルを開く方法（anything-filelist+）

    $ cd ~/.emacs.d/scripts
    $ ./install-make-filelist.sh

詳細は、[/scripts/](tree/master/scripts)を参考


## 3. ローマ字入力で日本語インクリメンタルサーチ（migemo）

    $ cd ~/.emacs.d/scripts
    $ ./install-migemo.sh

詳細は、[/scripts/](tree/master/scripts)を参考


## 4. ddskkのインストールと設定

不要な場合は、init.elの以下をコメントアウトする

	(require 'ddskk-settings)

インストール方法は、[/scripts/](tree/master/scripts)を参考

### 権限問題

SKKを利用してからEmacsを終了すると、cannot load ~/.skk* というエラーが出る場合があるので、
該当ファイルの権限を修正することで解決できる

	> chown +rw .skk*



## 5. Markdown

~/.emacs.d/el-get-init-files/init-markdown-mode.el に以下の設定を行うことで、  
[Marked 2](http://marked2app.com/) をMarkdown Viewerとして設定している
Marked 2は有料であるため、不要であるか別のViewerを利用する場合は、該当箇所を必要に応じて変更する

    (custom-set-variables
     '(markdown-open-command "~/.emacs.d/scripts/marked.sh"))

---


## 6. emacsclientの設定

eshellを使わない人は、emacsclientの設定をオススメする  
emacsclientを使うと、コマンドで指定したファイルを既に開いているemacsで開くことができる

### ~/.emacs.d/elisp/settings.elに以下を追加

	(if window-system (server-start))


### 利用するシェルにaliasを通す

	alias emacsclient='/Applications/Emacs.app/Contents/MacOS/bin/emacsclient'


### 使い方

	$ emacsclient [filename]
