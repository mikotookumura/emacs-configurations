(setq display-buffer-function 'popwin:display-buffer)

;; M-x anything
(push '("^\*anything.*\*$" :regexp t :width 0.5 :position right) popwin:special-display-config)
;; (setq anything-samewindow nil)
;; (push '("*anything*" :height 50) popwin:special-display-config)

;; M-!
(push "*Shell Command Output*" popwin:special-display-config)

;; M-x compile
(push '(compilation-mode :noselect t) popwin:special-display-config)

;; undo-tree
(push '(" *undo-tree*" :width 0.35 :position right) popwin:special-display-config)

;; direx (when user direx)
(push '(direx:direx-mode :position left :width 25 :dedicated t)
      popwin:special-display-config)
(global-set-key (kbd "C-x C-n") 'direx:jump-to-directory-other-window)

;; M-x dired-jump-other-window
(push '(dired-mode :position top) popwin:special-display-config)

;; escreen
(push '("*Escreen List*" :width 0.2 :position right) popwin:special-display-config)


;; Magit
;;(push '("^\*magit-edit.*\*$" :regexp t :height 0.5) popwin:special-display-config)
