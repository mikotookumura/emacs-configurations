;; tab幅を2にする
(defun coffee-custom ()
  "coffee-mode-hook"
  (and (set (make-local-variable 'tab-width) 2)
       (set (make-local-variable 'coffee-tab-width) 2))
  )
(add-hook 'coffee-mode-hook
  '(lambda() (coffee-custom)))
;; coffee-mode used to offer automatic deletion of trailing whitespace.
;; This is now left to whitespace-mode
;; (setq whitespace-action '(auto-cleanup)) ;; automatically clean up bad whitespace
;;(setq whitespace-style '(trailing space-before-tab indentation empty space-after-tab)) ;; only show bad whitespace


;; for auto-complete
(setq ac-modes (append '(coffee-mode)))
