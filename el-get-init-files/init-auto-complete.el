(add-to-list 'ac-dictionary-directories "~/.emacs.d/el-get/auto-complete/dict")
(require 'auto-complete-config)
(ac-config-default)

(global-auto-complete-mode t)

;; 背景色
(set-face-background 'ac-candidate-face "mintcream")
;; 下線
(set-face-underline 'ac-candidate-face "steelblue")
;; 前景色（選択）
(set-face-background 'ac-selection-face "DarkSlateGray")

;; auto-complete: 補完リストをCtrl-n, Ctrl-pで上下
(setq ac-use-menu-map t)
;; デフォルトで設定済み
(define-key ac-menu-map "\C-n" 'ac-next)
(define-key ac-menu-map "\C-p" 'ac-previous)

;; 補完選択時にTABがRETの挙動に変化する
;; 補完候補が一つしかないときにTABをするとRETの挙動になる
(setq ac-dwim t)

;; デフォルトの情報源
(setq-default ac-sources '(ac-source-words-in-all-buffer))


;; enabled in coding-mode
(defun my-coding-mode ()
  (setq ac-sources '(
                     ac-source-yasnippet
                     ac-source-words-in-same-mode-buffers)))
(add-hook 'html-mode-hook 'my-coding-mode)
(add-hook 'js2-mode-hook  'my-coding-mode)

(defvar ac-source-css-property-names
  '((candidates . (loop for property in ac-css-property-alist
                        collect (car property)))))
(defun my-css-mode-hook ()
  (setq ac-sources '(
                     ac-source-css-property
                     ac-source-css-property-names
                     ac-source-yasnippet
                     ac-source-words-in-same-mode-buffers)))
(add-hook 'css-mode-hook 'my-css-mode-hook)


(setq ac-modes (append '(html-mode css-mode js2-mode)))


;; use tern-mode as an ac resource (tern is intellligent js tooling)
(eval-after-load 'tern
   '(progn
      (require 'tern-auto-complete)
      (tern-ac-setup)))
