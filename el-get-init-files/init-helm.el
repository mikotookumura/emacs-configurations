;; activate helm-migemo
(helm-migemo-mode 1)

;; find files
(global-set-key (kbd "C-x C-f") 'helm-find-files)

;; filelist
(global-set-key (kbd "C-;") 'helm-for-files)

;; M-x
(global-set-key (kbd "M-x") 'helm-M-x)

;; bookmarks
(global-set-key (kbd "C-+") 'helm-bookmarks)

;; all-mark-rings
(global-set-key (kbd "M-SPC") 'helm-all-mark-rings)

;; find a file in the current git repo. with helm
(global-set-key (kbd "C-:") 'helm-ls-git-ls)

;; git grep with helm
(global-set-key (kbd "C-*") 'helm-cmd-t-grep)

;; kill-ringの最大値 Default=30
(setq kill-ring-max 20)
(setq helm-kill-ring-threshold 20)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)

;; tab in minibuffer
(define-key helm-find-files-map (kbd "TAB") 'helm-execute-persistent-action)
(define-key helm-read-file-map (kbd "TAB") 'helm-execute-persistent-action)

;; Emulate `kill-line' in helm minibuffer
(setq helm-delete-minibuffer-contents-from-point t)
(defadvice helm-delete-minibuffer-contents (before helm-emulate-kill-line activate)
  "Emulate `kill-line' in helm minibuffer"
  (kill-new (buffer-substring (point) (field-end))))
