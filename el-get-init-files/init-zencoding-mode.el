;; support for HTML5 tags
(when (require 'zencoding-mode nil t)
  (setq zencoding-block-tags
        (append (list
                 "article"
                 "section"
                 "aside"
                 "nav"
                 "figure"
                 "address"
                 "header"
                 "footer")
                zencoding-block-tags))
  (setq zencoding-inline-tags
        (append (list
                 "textarea"
                 "small"
                 "time" "del" "ins"
                 "sub"
                 "sup"
                 "i" "s" "b"
                 "ruby" "rt" "rp"
                 "bdo"
                 "iframe" "canvas"
                 "audio" "video"
                 "ovject" "embed"
                 "map"
                 )
                zencoding-inline-tags))
  (setq zencoding-self-closing-tags
        (append (list
                 "wbr"
                 "object"
                 "source"
                 "area"
                 "param"
                 "option"
                 )
                zencoding-self-closing-tags))
)

(add-hook 'sgml-mode-hook 'zencoding-mode)
(add-hook 'html-mode-hook 'zencoding-mode)
(add-hook 'text-mode-hook 'zencoding-mode)

;; zencoding expand with yasnippet
(define-key zencoding-mode-keymap (kbd "M-m") 'zencoding-expand-yas)
(define-key zencoding-preview-keymap (kbd "C-m") 'zencoding-expand-yas)

;; zencodeing preview
(define-key zencoding-mode-keymap (kbd "M-p") 'zencoding-expand-line)
