(require 'eshell)

;; set shell-switcher-mode
(setq shell-switcher-mode t)


;; 環境変数設定
(setenv "PATH" (concat
                "/usr/local/bin:"
                "/usr/X11/bin:"
                (concat (getenv "HOME") "/.nodebrew/current/bin:")
                (concat (getenv "HOME") "/Documents/bin:")
                (concat (getenv "HOME") "/Documents/Android/adt-bundle-mac-x86_64-20140321/sdk/platform-tools:")
                (concat (getenv "HOME") "/node_modules/.bin:")
                (concat (getenv "HOME") "/Library/Python/2.7/bin:")
                "/Applications/google-cloud-sdk/bin/:"
                "/opt/local/bin:"
                "/opt/local/sbin:"
                (getenv "PATH")))
(setenv "NODE_PATH" (concat
                     (concat (getenv "HOME") "/.npm/lib:")
                     (concat (getenv "HOME") "/local/node:")
                     (concat (getenv "HOME") "/local/node/lib/node_modules")
                     ))
;; change PAGER (default is man)
;; lessやgitコマンド利用時に、「:」が毎行残る問題を解決
(setenv "PAGER" "cat")


;; 環境変数を基に、eshellサーチパスを設定する
(setq-default eshell-path-env (getenv "PATH"))

;; 環境変数の内、PATH のみは exec-path も設定する必要がある
(setq exec-path (parse-colon-path (getenv "PATH")))



;; 補完時に大文字小文字を区別しない
(setq eshell-cmpl-ignore-case t)
;; 確認なしでヒストリ保存
(setq eshell-ask-to-save-history (quote always))
;; 補完時にサイクルする
;; (setq eshell-cmpl-cycle-completions t)
;; (setq eshell-cmpl-cycle-completions nil)
;; 補完候補がこの数値以下だとサイクルせずに候補表示
;; (setq eshell-cmpl-cycle-cutoff-length 5)
;; 履歴で重複を無視する
(setq eshell-hist-ignoredups t)


;; カスタム変数の設定
(custom-set-variables
 '(eshell-history-size 1000)
 '(eshell-modules-list
   '(eshell-alias
     eshell-banner
     eshell-basic
     eshell-cmpl
     eshell-dirs
     eshell-glob
     eshell-hist
     eshell-ls
     eshell-pred
     eshell-prompt
     eshell-script
     eshell-term
     eshell-unix
     ))
 ;; If non-nil, [shell-command] will use Eshell instead of shell-mode.
 '(eshell-prefer-to-shell t nil (eshell))
 ;; Name to use for the TERM variable when running visual commands
 '(eshell-term-name "ansi")

 ;; lessやgitコマンド利用時に、「:」が毎行残る問題を解決
 '(eshell-visual-commands (quote ("vi" "vim" "top" "screen" "lynx"
                                  "ssh" "rlogin" "telnet" "bash"
                                  "less" "more" "man"
                                  "irb" "coffee" "ghci")))
 )

;; color
(require 'ansi-color)
(defun eshell-handle-ansi-color ()
  (ansi-color-apply-on-region eshell-last-output-start
                              eshell-last-output-end))
(add-to-list 'eshell-output-filter-functions 'eshell-handle-ansi-color)

;; C-aで行頭に行かずに"$ "の次にカーソルが来るように
;; C-p, C-nで履歴をループ.
(add-hook 'eshell-mode-hook
          '(lambda ()
             (progn
               (define-key eshell-mode-map "\C-a" 'eshell-bol)
               (define-key eshell-mode-map "\C-p"
                 'eshell-previous-matching-input-from-input)
               (define-key eshell-mode-map "\C-n"
                 'eshell-next-matching-input-from-input)
               )
             ))

(eval 'eshell-modules-list)
;; プロンプトの表示変更
(defun my-eshell-prompt ()
  (concat (eshell/pwd) "\n→ " ))
(setq eshell-prompt-function 'my-eshell-prompt)
(setq eshell-prompt-regexp "^[^#$n]*[#→] ")

;; sudoに続くコマンドの変換
(defun pcomplete/sudo ()always
  "Completion rules for the `sudo' command."
  (let ((pcomplete-help "complete after sudo"))
    (pcomplete-here (pcomplete-here (eshell-complete-commands-list)))))

;; eshell/clear
(defun eshell/clear ()
  "Clear the current buffer, leaving one prompt at the top."
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)))

;; eshell/bmk - version 0.1.3
(defun pcomplete/eshell-mode/bmk ()
  "Completion for `bmk'"
  (pcomplete-here (bookmark-all-names)))
(defun eshell/bmk (&rest args)
  "Integration between EShell and bookmarks.
For usage, execute without arguments."
  (setq args (eshell-flatten-list args))
  (let ((bookmark (car args))
        filename name)
    (cond
     ((eq nil args)
      (format "Usage:
* bmk BOOKMARK to
** either change directory pointed to by BOOKMARK
** or bookmark-jump to the BOOKMARK if it is not a directory.
* bmk . BOOKMARK to bookmark current directory in BOOKMARK.
Completion is available."))
     ((string= "." bookmark)
      ;; Store current path in EShell as a bookmark
      (if (setq name (car (cdr args)))
          (progn
            (bookmark-set name)
            (bookmark-set-filename name (eshell/pwd))
            (format "Saved current directory in bookmark %s" name))
        (error "You must enter a bookmark name")))
     (t
      ;; Check whether an existing bookmark has been specified
      (if (setq filename (cdr (car (bookmark-get-bookmark-record bookmark))))
          ;; If it points to a directory, change to it.
          (if (file-directory-p filename)
              (eshell/cd filename)
            ;; otherwise, just jump to the bookmark
            (bookmark-jump bookmark))
        (error "%s is not a bookmark" bookmark))))))


;; eshell での補完に auto-complete.el を使う
;; (require 'pcomplete)
;; (add-to-list 'ac-modes 'eshell-mode)
;; (ac-define-source pcomplete
;;   '((candidates . pcomplete-completions)))
;; (defun my-ac-eshell-mode ()
;;   (setq ac-sources
;;         '(ac-source-pcomplete
;;           ac-source-dictionary)))
;; (add-hook 'eshell-mode-hook
;;           (lambda ()
;;             (my-ac-eshell-mode)
;;             (define-key eshell-mode-map (kbd "C-i") 'auto-complete)))
(defun ac-pcomplete ()
  ;; eshell uses `insert-and-inherit' to insert a \t if no completion
  ;; can be found, but this must not happen as auto-complete source
  (flet ((insert-and-inherit (&rest args)))
    ;; this code is stolen from `pcomplete' in pcomplete.el
    (let* (tramp-mode ;; do not automatically complete remote stuff
           (pcomplete-stub)
           (pcomplete-show-list t) ;; inhibit patterns like * being deleted
           pcomplete-seen pcomplete-norm-func
           pcomplete-args pcomplete-last pcomplete-index
           (pcomplete-autolist pcomplete-autolist)
           (pcomplete-suffix-list pcomplete-suffix-list)
           (candidates (pcomplete-completions))
           (beg (pcomplete-begin))
           ;; note, buffer text and completion argument may be
           ;; different because the buffer text may bet transformed
           ;; before being completed (e.g. variables like $HOME may be
           ;; expanded)
           (buftext (buffer-substring beg (point)))
           (arg (nth pcomplete-index pcomplete-args)))
      ;; we auto-complete only if the stub is non-empty and matches
      ;; the end of the buffer text
      (when (and (not (zerop (length pcomplete-stub)))
                 (or (string= pcomplete-stub ; Emacs 23
                              (substring buftext
                                         (max 0
                                              (- (length buftext)
                                                 (length pcomplete-stub)))))
                     (string= pcomplete-stub ; Emacs 24
                              (substring arg
                                         (max 0
                                              (- (length arg)
                                                 (length pcomplete-stub)))))))
        ;; Collect all possible completions for the stub. Note that
        ;; `candidates` may be a function, that's why we use
        ;; `all-completions`.
        (let* ((cnds (all-completions pcomplete-stub candidates))
               (bnds (completion-boundaries pcomplete-stub
                                            candidates
                                            nil
                                            ""))
               (skip (- (length pcomplete-stub) (car bnds))))
          ;; We replace the stub at the beginning of each candidate by
          ;; the real buffer content.
          (mapcar #'(lambda (cand) (concat buftext (substring cand skip)))
                  cnds))))))

(defvar ac-source-pcomplete
  '((candidates . ac-pcomplete)))

(add-hook 'eshell-mode-hook #'(lambda () (setq ac-sources '(ac-source-pcomplete))))
(add-to-list 'ac-modes 'eshell-mode)


;; helm で履歴から入力
(add-hook 'eshell-mode-hook
          #'(lambda ()
              (define-key eshell-mode-map
                (kbd "M-h")
                'helm-eshell-history)))
;; helm で補完
(add-hook 'eshell-mode-hook
          #'(lambda ()
              (define-key eshell-mode-map
                (kbd "M-c")
                'helm-esh-pcomplete)))

;;;; following key bindgs are automatically defined
;; @@ shell-switcher: 複数のeshell起動
;; C-' opens the first 2 buffers (one after the other) and switch between them
;; Repeating ' continues switching after an initial C-'
;; (define-key shell-switcher-mode-map (kbd "C-'")
;;             'shell-switcher-switch-buffer)
;; ;; C-x 4 ' is similar to C-' but within another window
;; (define-key shell-switcher-mode-map (kbd "C-x 4 '")
;;             'shell-switcher-switch-buffer-other-window)
;; ;; C-M-' forces the creation of a new shell
;; (define-key shell-switcher-mode-map (kbd "C-M-'")
;;             'shell-switcher-new-shell)
