;; use rvm's default ruby for the current Emacs session
(rvm-use-default)

;; rvm-autodetect-ruby will add a ruby-mode-hook which will call rvm-activate-corresponding-ruby to detect the Ruby version.
(rvm-autodetect-ruby)
