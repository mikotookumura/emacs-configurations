(provide 'settings)

;;;;;;;; Global Environment ;;;;;;;;;


;; 環境変数
(setenv "LANG" "ja_JP.UTF-8")

;; 日本語設定
(set-language-environment 'Japanese)
(set-language-environment 'utf-8)
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-clipboard-coding-system 'utf-8)
;; Macの場合ファイル名はutf-8-hfs
;;(setq file-name-coding-system 'utf-8)
;;(setq locale-coding-system 'utf-8)
(if (eq window-system 'ns)
   (progn
     (require 'ucs-normalize)
     (setq file-name-coding-system 'utf-8-hfs)
     (setq locale-coding-system 'utf-8-hfs)))

;; 新しいウィンドウで開くのを禁止
(setq ns-pop-up-frames nil)

;; find-fileのデフォルトディレクトリ
(setq default-directory "~/")
(setq command-line-default-directory "~/")

;; 終了時にオートセーxブファイル（#filename#）を消す
(setq delete-auto-save-files t)
;; オートセーブ間隔 :Default=30sec
(setq auto-save-timeout 600)
;; オートセーブ間隔 :Default=300type
(setq auto-save-interval 3000)

;; バックアップファイル（filename~）とオートセーブファイル（#filename#）は~/.emacs.d/backup/に保存
;; 下記コマンドを crontab -e で記述
;; 0 1 * * * find ~/.emacs.d/backup -mtime +30 -exec rm -f {} \;
(setq auto-save-file-name-transforms
      `((".*" ,(expand-file-name "~/.emacs.d/backup/") t)))
(setq make-backup-files t)
(setq backup-directory-alist
      (cons (cons "\\.*$" (expand-file-name "~/.emacs.d/backup"))
            backup-directory-alist))

;; 元ファイルをバックアップファイルにコピーしてから、新たな内容で上書きする
;; つまり、ファイルの所有者とグループは変わらない
;; デフォルトでは、元ファイルを改名してバックアップとして保存し、新たな内容の新たなファイルを作成する
(setq backup-by-copying t)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.

 ;; ロックファイル（.#filename）を作成しない
 ;; （ロックファイルは同時編集を防ぐために作成されるsimlink）
 ;; does not create lock file
 '(create-lockfiles nil)
 ;; ゴミ箱を利用
 '(delete-by-moving-to-trash t)
 '(trash-directory "~/.Trash")
 ;; ツールバー削除
 '(tool-bar-mode nil)
 ;; 基本設定
 '(display-time-mode t)
 '(inhibit-startup-screen t)
 '(safe-local-variable-values
   (quote
    ((eval ignore-errors "Write-contents-functions is a buffer-local alternative to before-save-hook"
           (add-hook
            (quote write-contents-functions)
            (lambda nil
              (delete-trailing-whitespace)
              nil))
           (require
            (quote whitespace))
           "Sometimes the mode needs to be toggled off and on."
           (whitespace-mode 0)
           (whitespace-mode 1))
     (whitespace-line-column . 80)
     (whitespace-style face tabs trailing lines-tail))))
 '(transient-mark-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;; スクロール調整
(mouse-wheel-mode)
(global-set-key [wheel-up]
                '(lambda () "" (interactive) (scroll-down 1)))
(global-set-key [wheel-down]
                '(lambda () "" (interactive) (scroll-up 1)))
(global-set-key [double-wheel-up]
                '(lambda () "" (interactive) (scroll-down 1)))
(global-set-key [double-wheel-down]
                '(lambda () "" (interactive) (scroll-up 1)))
(global-set-key [triple-wheel-up]
                '(lambda () "" (interactive) (scroll-down 2)))
(global-set-key [triple-wheel-down]
                '(lambda () "" (interactive) (scroll-up 2)))

;; Use the clipboard, pretty please, so that copy/paste "works"
;; (setq x-select-enable-clipboard t)

;; whenever an external process changes a file underneath emacs, and there
;; was no unsaved changes in the corresponding buffer, just revert its
;; content to reflect what's on-disk.
(global-auto-revert-mode 1)

;; ;; avoid compiz manager rendering bugs
;; (add-to-list 'default-frame-alist '(alpha . 100))

;; cua-mode
(cua-mode)
(setq cua-enable-cua-keys nil) ;; disable cua-mode key bindings

;; emacsclient
(if window-system (server-start))



;;;;;;; Basic Typing Rules ;;;;;;;;

;; 英数は半角に
(setq skk-number-style nil)

;; Undo回数制限
(setq undo-limit 100000)
(setq undo-strong-limit 130000)

;; 行の先頭でC-kを一回押すだけで行全体を消去する
(setq kill-whole-line t)

;; 最終行に必ず一行挿入する
(setq require-final-newline t)

;; バッファの最後でnewlineで新規行を追加するのを禁止する
(setq next-line-add-newlines nil)



;;;;;;;; requires ;;;;;;;;;

;; Tramp
(require 'tramp)
(setq tramp-default-method "ssh")

;; doc-view-mode
(require 'doc-view)
(add-hook 'doc-view-mode-hook 'auto-revert-mode)
(setq doc-view-continuous t)

;; eldoc
(require 'eldoc)

;;;;;;;; languages ;;;;;;;;;

;; shell
(setq sh-basic-offset 2)
(setq sh-indentation 2)


;; @@ eco templates
(add-to-list 'auto-mode-alist '("\\.eco\\'" . html-mode))


;; YaTex
(setq auto-mode-alist
      (cons (cons "\\.tex$" 'yatex-mode) auto-mode-alist))
(autoload 'yatex-mode "yatex" "Yet Another LaTeX mode" t)

;; プレビュー(TeXShop の使用)
;(setq tex-command "~/Library/TeXShop/bin/platex2pdf-utf8" dvi2-command "open -a TexShop")

;; 文章作成時の漢字コードの設定
;; 1 = Shift_JIS, 2 = ISO-2022-JP, 3 = EUC-JP
;; default は 2
(setq YaTeX-kanji-code 1) ; euc-jp

;; TeX command
;(setq tex-command "latex")
(setq tex-command "Xtexshop")

;; DVI preview command
(setq dvi2-command "open -a Preview")
;;(setq dvi2-command "open -a TexShop")
;(setq dvi2-command "xdvi")
;(setq dvi2-command "dvips")
;(setq dvi2-command "dvipdfm")
;(setq dvi2-command "dvipdfmx")

;; BIBTeX command
;(setq bibtex-command "bibtex")
(setq bibtex-command "jbibtex")

;; MakeIndex command
(setq makeindex-command "makeindex")
;(setq makeindex-command "mendex")

;; Error Message code (utf-8)
(setq YaTeX-latex-message-code 'utf-8)


;;;;;;;; Apple Script ;;;;;;;;;

;; Safari ;;;;;;;;;;;;;

;; (defun safari-do-javascript(command)
;;   (do-applescript
;;    (concat "
;; tell application \"Safari\"
;; if count of document is greater than 0 then
;; do JavaScript \"" command "\" in document 1
;; end if
;; end tell")))

;; (defun safari-scroll(direction)
;;   (let ((amount 50))
;;     (if (eq direction 1)
;;         (setq amount (- amount)))
;;     (safari-do-javascript
;;      (concat "javascript:window.scrollBy(0,"
;;              (int-to-string amount)
;;              ");"))))

;; (defun safari-scroll-up()
;;   (interactive)
;;   (safari-scroll 1))
;; (global-set-key (kbd "C-M-p") 'safari-scroll-up)

;; (defun safari-scroll-down()
;;   (interactive)
;;   (safari-scroll 0))
;; (global-set-key (kbd "C-M-n") 'safari-scroll-down)

;; (defun safari-reload()
;;   (interactive)
;;   (safari-do-javascript "location.reload(true);"))
;; (global-set-key (kbd "C-M-r") 'safari-reload)

;; (defun safari-next-tab ()
;;   (interactive)
;;   (shell-command "osascript -e \"tell application \\\"Safari\\\" to tell window 1 to set current tab to tab (((current tab's index) mod (count every tab)) + 1)\""))
;; (global-set-key (kbd "C-M-f") 'safari-next-tab)

;; (defun safari-previous-tab ()
;;   (interactive)
;;   (shell-command "osascript -e \"tell application \\\"Safari\\\"
;; 	tell window 1
;; 		set act_i to current tab's index
;; 		if act_i > 1 then
;; 			set (current tab) to tab (act_i - 1)
;; 		else
;; 			set current tab to tab (get count every tab)
;; 		end if
;; 	end tell
;; end tell\""))
;; (global-set-key (kbd "C-M-b") 'safari-previous-tab)


;; (defun safari-history-forward()
;;   (interactive)
;;   (safari-do-javascript "history.forward();"))
;; (global-set-key '[M-right] 'safari-history-forward)

;; (defun safari-history-back()
;;   (interactive)
;;   (afari-do-javascript "history.back();"))
;; (global-set-key '[M-left] 'safari-history-back)


;; Chrome ;;;;;;;;;;;;;

(defun chrome-reload ()
  (interactive)
  (shell-command "osascript -e 'tell application \"Google Chrome\" to reload active tab of first window'"))
(global-set-key (kbd "C-M-r") 'chrome-reload)

(defun chrome-next-tab ()
  (interactive)
  (shell-command "osascript -e 'tell application \"Google Chrome\" to set active tab index of first window to (get (active tab index of first window) mod (get count tabs of first window)) + 1'"))
(global-set-key (kbd "C-M-f") 'chrome-next-tab)

(defun chrome-previous-tab ()
  (interactive)
  (shell-command "osascript -e 'tell application \"Google Chrome\"
	set act_i to active tab index of first window
	if act_i > 1 then
		set (active tab index of first window) to (get active tab index of first window) - 1
	else
		set active tab index of first window to get count tabs of first window
	end if
end tell'"))
(global-set-key (kbd "C-M-b") 'chrome-previous-tab)

(defun chrome-scroll-down ()
  (interactive)
  (shell-command "osascript -e 'tell application \"Google Chrome\" to execute active tab of first window javascript \"var x = document.documentElement.scrollLeft || document.body.scrollLeft; var y = document.documentElement.scrollTop || document.body.scrollTop; y += 50; window.scroll(x, y);\"'"))
(global-set-key (kbd "C-M-n") 'chrome-scroll-down)

(defun chrome-scroll-up ()
  (interactive)
  (shell-command "osascript -e 'tell application \"Google Chrome\" to execute active tab of first window javascript \"var x = document.documentElement.scrollLeft || document.body.scrollLeft; var y = document.documentElement.scrollTop || document.body.scrollTop; y -= 50; window.scroll(x, y);\"'"))
(global-set-key (kbd "C-M-p") 'chrome-scroll-up)



;;;;;;;;; filechache ;;;;;;;;;;


(require 'filecache)

(file-cache-add-directory-list
 (list "~" "~/.emacs.d" "~/Documents/git" "~/Sites/")) ;; ディレクトリを追加
;; (file-cache-add-file-list
;;  (list "~/memo/memo.txt")) ;; ファイルを追加

(define-key minibuffer-local-completion-map "\C-c\C-i"
  'file-cache-minibuffer-complete)

;; Add to file-cache when `kill-buffer'
(defun file-cache-add-this-file ()
  (and buffer-file-name
       (file-exists-p buffer-file-name)
       (file-cache-add-file buffer-file-name)))
(add-hook 'kill-buffer-hook 'file-cache-add-this-file)




;;;;;;;;; dired ;;;;;;;;;;

;; C-x C-j opens dired with the cursor right on the file you're editing
(require 'dired-x)

;; dired で Quick Look
(defun my-dired-do-quicklook ()
  "In dired, preview with Quick Look."
  (interactive)
  (let ((file (dired-get-filename))
        (process (get-process "qlmanage_ps")))
    (if process
        (kill-process process)
      (start-process "qlmanage_ps" nil "qlmanage" "-p" file))))
(add-hook 'dired-mode-hook
          '(lambda ()
             (define-key dired-mode-map (kbd "SPC") 'my-dired-do-quicklook)
             ))

;; ファイル操作のデフォルトターゲットパスを隣りの別diredバッファにする
(setq dired-dwim-target t)
;; ディレクトリから先に表示
(setq ls-lisp-dirs-first t)
;; 再帰コピー
(setq dired-recursive-copies 'always)
;; 再帰削除
(setq dired-recursive-deletes 'always)
;; @@ diredでディレクトリを開いた際に新規タブで開くのをやめる
;; ファイルなら別バッファで、ディレクトリなら同じバッファで開く
(defun dired-open-in-accordance-with-situation ()
  (interactive)
  (let ((file (dired-get-filename)))
    (if (file-directory-p file)
        (dired-find-alternate-file)
      (dired-find-file))))
;; dired-up-directory(ペアレントディレクトリに移動)の際にも、別タブで開かない
(defun dired-up-directory (&optional other-window)
          "Run Dired on parent directory of current directory."
          (interactive "P")
          (let* ((dir (dired-current-directory))
     	    (orig (current-buffer))
     	    (up (file-name-directory (directory-file-name dir))))
            (or (dired-goto-file (directory-file-name dir))
     	   ;; Only try dired-goto-subdir if buffer has more than one dir.
     	   (and (cdr dired-subdir-alist)
     		(dired-goto-subdir up))
     	   (progn
     	     (kill-buffer orig)
     	     (dired up)
     	     (dired-goto-file dir)))))
;; dired-find-alternate-file の有効化
(put 'dired-find-alternate-file 'disabled nil)
;; RET 標準の dired-find-file では dired バッファが複数作られるので
;; dired-find-alternate-file を代わりに使う
(define-key dired-mode-map (kbd "RET") 'dired-open-in-accordance-with-situation)
(define-key dired-mode-map (kbd "a") 'dired-find-file)
;; ディレクトリの移動キーを追加(wdired 中は無効)
;; (define-key dired-mode-map (kbd "<left>") 'dired-up-directory)
(define-key dired-mode-map (kbd "<left>") 'dired-up-directory)
(define-key dired-mode-map (kbd "C-b") 'dired-up-directory)
(define-key dired-mode-map (kbd "<right>") 'dired-open-in-accordance-with-situation)
(define-key dired-mode-map (kbd "C-f") 'dired-open-in-accordance-with-situation)

;; ゴミ箱を利用する
(custom-set-variables
 '(delete-by-moving-to-trash t)
 '(trash-directory "~/.Trash"))

;; for sorting by extensions
(when (eq system-type 'darwin)
  (require 'ls-lisp)
  (setq ls-lisp-use-insert-directory-program nil))


;;;;;;;; eww ;;;;;;;

(setq eww-search-prefix "https://www.google.co.jp/search?q=")


;;;;;;;; Key Binds ;;;;;;;

;; Ctrl-h → backspace
(keyboard-translate ?\C-h ?\C-?)
(global-set-key "\C-h" nil)

;; 範囲指定していないとき、C-wで前の単語を削除
(defadvice kill-region (around kill-word-or-kill-region activate)
  (if (and (called-interactively-p 'interactive) transient-mark-mode (not mark-active))
      (backward-kill-word 1)
    ad-do-it))
;; (defadvice kill-region (around kill-word-or-kill-region activate)
;;   (if (and (interactive-p) transient-mark-mode (not mark-active))
;;       (backward-kill-word 1)
;;     ad-do-it))
;; minibuffer用
(define-key minibuffer-local-completion-map "\C-w" 'backward-kill-word)

;; カーソル位置の単語を削除
(defun kill-word-at-point ()
  (interactive)
  (let ((char (char-to-string (char-after (point)))))
    (cond
     ((string= " " char) (delete-horizontal-space))
     ((string-match "[\t\n -@\[-`{-~]" char) (kill-word 1))
     (t (forward-char) (backward-word) (kill-word 1)))))
(global-set-key "\M-d" 'kill-word-at-point)

;; kill-lineで行が連結したときにインデントを減らす
(defadvice kill-line (before kill-line-and-fixup activate)
  (when (and (not (bolp)) (eolp))
    (forward-char)
    (fixup-whitespace)
    (backward-char)))

;; Navigate windows with M-<arrows>
(windmove-default-keybindings 'meta)
(setq windmove-wrap-around t)

;; 変更部分を可視化/不可視化のトグル
(global-set-key (kbd "s-h") 'highlight-changes-visible-mode)
;; 次の変更箇所に移動
(global-set-key (kbd "M-]") 'highlight-changes-next-change)
;; 前の変更箇所に移動
(global-set-key (kbd "M-[") 'highlight-changes-previous-change)

;; タブ文字、全角空白、文末の空白の色付け
;; 設定自体は下でしてるので注意
;; (global-whitespace-mode 1) 常に whitespace-mode だと動作が遅くなる場合がある
(global-set-key (kbd "C-x w") 'global-whitespace-mode)

;; diredから"r"でファイル名をインライン編集する
(require 'wdired)
(define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)


;; kill all buffers
(defun kill-all-buffers()
  (interactive)
  (yes-or-no-p "kill all buffers? ")
  (dolist (buf (buffer-list))
    (unless (member (buffer-name) '("*scratch*" "*Messages*"))
      (kill-buffer buf))))
(global-set-key (kbd "C-x C-k") 'kill-all-buffers)

;; under mac, have Command as Meta and keep Option for localized input
(when (string-match "apple-darwin" system-configuration)
  (setq mac-allow-anti-aliasing t)
;;   (setq mac-command-modifier 'meta)
;;   (setq mac-option-modifier 'none)
  )

;; winner-mode provides C-<left> to get back to previous window layout
;; (winner-mode 1)




;;;;;;; Looks ;;;;;;;;

;;;; elisp > color-theme でテーマ設定してる

;; （行，列）をステータスバーに表示
;; (setq line-number-mode t)
(line-number-mode 1)			; have line numbers and
(column-number-mode 1)			; column numbers in the mode line

;; スクロールバーを削除
(scroll-bar-mode -1)

;; Carbon Emacsの設定で入れられた. メニューを隠したり．
(custom-set-variables
 '(display-time-mode t)
 '(tool-bar-mode nil)
 '(transient-mark-mode t))
(custom-set-faces)

;; タイトルバーにファイルパス表示
(setq frame-title-format (format "%%f - Emacs@%s" (system-name)))

;; 対応する括弧を光らせる。
(show-paren-mode 1)

;; ウィンドウ内に収まらないときだけ括弧内も光らせる。
(setq show-paren-style 'mixed)

;; タブを2個のスペースに変換
(setq-default tab-width 2 indent-level 2 indent-tabs-mode nil)

;; line settings
(global-hl-line-mode)			; highlight current line;; To customize the background color
(set-face-background 'hl-line "#151a1b") ;; highlight color
(global-linum-mode 1)			; add line numbers on the left

;; 透明化
;; (add-to-list 'default-frame-alist '(alpha . (90 80)))

;; タブ文字、全角空白、文末の空白の色付け
;; @see http://www.emacswiki.org/emacs/WhiteSpace
;; @see http://xahlee.org/emacs/whitespace-mode.html
(require 'whitespace)
(setq whitespace-style '(spaces tabs space-mark tab-mark))
(setq whitespace-display-mappings
      '(
        ;; (space-mark 32 [183] [46]) ; normal space, ·
        (space-mark 160 [164] [95])
        (space-mark 2208 [2212] [95])
        (space-mark 2336 [2340] [95])
        (space-mark 3616 [3620] [95])
        (space-mark 3872 [3876] [95])
        (space-mark ?\x3000 [?\□]) ;; 全角スペース
        ;; (newline-mark 10 [182 10]) ; newlne, ¶
        (tab-mark 9 [9655 9] [92 9]) ; tab, ▷
        ))

;; 行末の空白を表示
(setq-default show-trailing-whitespace t)
;; EOB を表示
(setq-default indicate-empty-lines t)
(setq-default indicate-buffer-boundaries 'left)

;; マーク領域を色付け
(setq transient-mark-mode t)

;; 変更点に色付け
(global-highlight-changes-mode t)
;; 初期は非表示として highlight-changes-visible-mode で表示する
(setq highlight-changes-visibility-initial-state nil)

;; GUIで起動した際に出るチュートリアル画面を消去
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;フォント
(create-fontset-from-ascii-font "Menlo-14:weight=normal:slant=normal" nil "menlomarugo")
(set-fontset-font "fontset-menlomarugo"
                  'unicode
                  (font-spec :family "Hiragino Maru Gothic ProN" :size 14)
                  nil
                  'append)
(add-to-list 'default-frame-alist '(font . "fontset-menlomarugo"))
