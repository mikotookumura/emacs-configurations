(provide 'ddskk-settings)


;;;;;; SKK Key Binds ;;;;;;;
;; C-mで改行してインデント，C-jでSKK起動
(global-set-key "\C-m" 'newline-and-indent)
(global-set-key "\C-j" 'skk-mode)
;; C-\ でも SKK に切り替えられるように設定
;; SKK を Emacs の input method として使用する
(setq default-input-method "japanese-skk")



;;;;;;;; External Plugins ;;;;;;;;;

;; @@ MIGEMO
;; [migemo]isearch - Turnign off IME
;; http://www.jaist.ac.jp/~n-yoshi/tips/elisp_tips.html#ime-off
(add-hook 'isearch-mode-hook
          (lambda () (skk-latin-mode-on)))

;; Zencoding
;; undefined C-j by overrideing skk-mode
(define-key zencoding-mode-keymap (kbd "C-j") 'skk-mode)

;; eshell
;; undefined C-j by overrideing skk-mode
(add-hook 'eshell-mode-hook
          #'(lambda ()
              (define-key eshell-mode-map
                (kbd "C-j")
                'skk-mode)))


;;;;;;;; Settings ;;;;;;;;;

;; 変換の学習
(require 'skk-study)

;; AquaSKK 対策
(setq mac-pass-control-to-system nil)


;; @@ 基本ルール

;; モードラインに SKK アイコンを表示する。
(setq skk-show-icon t)
;; ▽モードと▼モード時のアンドゥ情報を記録しない
(setq skk-undol-kakutei-word-only t)
;; 変換時に注釈 (annotation) を表示する
(setq skk-show-annotation t)
;; 変換候補一覧と注釈 (annotation) を GUI ぽく表示する
(setq skk-show-tooltip t)

;; Emacs 起動時に SKK を前もってロードする
(setq skk-preload t)
;; 注) skk.el をロードするだけなら (require 'skk) でもよい。上記設定の
;; 場合は、skk-search-prog-list に指定された辞書もこの時点で読み込んで
;; 準備する。Emacs の起動は遅くなるが，SKK を使い始めるときのレスポンス
;; が軽快になる。

;; AquaSKK と辞書を共用
;; skkserv
(setq skk-server-host "localhost"
      skk-server-portnum 1178
      skk-server-report-response t)
;; Dropbox中の変換候補辞書を参照
;; (add-to-list 'skk-completion-prog-list
;;              '(skk-comp-from-jisyo "~/Dropbox/Private/skk-jisyo.utf8"))


;; skkロード後処理
(add-hook 'skk-load-hook
	  (lambda ()
        ;; 文脈に応じた自動モード切り替え(プログラミングにおいて、コメントや文字列部のみ日本語入力にする)
  	    ;; (require 'context-skk)
        ;; 見出し語を補完して変換
        (define-key skk-j-mode-map (kbd "C-SPC") 'skk-start-henkan-with-completion)))


;; @@ 見た目
(when skk-use-color-cursor
  ;; カーソル色を変えてみる
  (setq skk-cursor-hiragana-color "SteelBlue"
        skk-cursor-katakana-color "red"
        skk-cursor-abbrev-color "red"
        skk-cursor-jisx0208-latin-color "red"
        skk-cursor-jisx0201-color "purple"
        skk-cursor-latin-color "white"))

;; 動的補完の複数表示群のフェイス
(set-face-foreground 'skk-dcomp-multiple-face "Black")
(set-face-background 'skk-dcomp-multiple-face "mintcream")
(set-face-bold-p 'skk-dcomp-multiple-face nil)
;; 動的補完の複数表示郡の補完部分のフェイス
(set-face-foreground 'skk-dcomp-multiple-trailing-face "dim gray")
(set-face-bold-p 'skk-dcomp-multiple-trailing-face nil)
;; 動的補完の複数表示郡の選択対象のフェイス
(set-face-foreground 'skk-dcomp-multiple-selected-face "White")
(set-face-background 'skk-dcomp-multiple-selected-face "DarkSlateGray")
(set-face-bold-p 'skk-dcomp-multiple-selected-face nil)



;; @@ 動的補完

;; 動的な補完を使う
(setq skk-dcomp-activate t)
;; ▽ほ-!-んとう の「んとう」の face
;;(set-face-foreground 'skk-dcomp-face "green")

;; 動的補完で候補を複数表示する （XEmacs では機能しません）
(setq skk-dcomp-multiple-activate t
      skk-dcomp-multiple-rows 7)
;; ローマ字 prefix をみて補完する
(setq skk-comp-use-prefix t)
;; 補完時にサイクルする
(setq skk-comp-circulate t)
;; 個人辞書の文字コードを指定する
(setq skk-jisyo-code 'utf-8)



;; @@ 変換動作の調整

;; 送り仮名が厳密に正しい候補を優先して表示する
(setq skk-henkan-strict-okuri-precedence t)

;; 辞書登録のとき、余計な送り仮名を送らないようにする
;;(setq skk-check-okurigana-on-touroku 'auto)
;;漢字登録時、送り仮名が厳密に正しいかをチェック
(setq skk-check-okurigana-on-touroku t)



;; @@ 入力ルール

;; かなモードの入力でモード変更を行わずに、数字入力中の
;; 小数点 (.) およびカンマ (,) 入力を実現する。
;; (例) かなモードのまま 1.23 や 1,234,567 などの記述を行える。
;; period
(setq skk-rom-kana-rule-list
	  (cons '("." nil skk-period)
			skk-rom-kana-rule-list))
(defun skk-period (arg)
  (let ((c (char-before (point))))
    (cond ((null c) "。")
          ((and (<= ?0 c) (>= ?9 c)) ".")
          ((and (<= ?０ c) (>= ?９ c)) "．")
          ((string= skk-kutouten-type 'en) "．")
          (t "。"))))

;; comma
(setq skk-rom-kana-rule-list
	  (cons '("," nil skk-comma)
			skk-rom-kana-rule-list))
(defun skk-comma (arg)
  (let ((c (char-before (point))))
    (cond ((null c) "、")
          ((and (<= ?0 c) (>= ?9 c)) ",")
          ((and (<= ?０ c) (>= ?９ c)) "，")
          ((string= skk-kutouten-type 'en) "，")
          (t "、"))))


;; かなモードの入力で (モード変更を行なわずに) 長音(ー)を
;; ASCII 数字の直後では `-' にする
(setq skk-rom-kana-rule-list
	  (cons '("-" nil skk-hyphen)
			skk-rom-kana-rule-list))
(defun skk-hyphen (arg)
  (let ((c (char-before (point))))
    (cond ((null c) "ー")
          ((and (<= ?0 c) (>= ?9 c)) "-")
          (t "ー"))))

;; 変換ルール
(setq skk-rom-kana-rule-list
      (append skk-rom-kana-rule-list
              '(
                ;; zコマンド
                ("z " nil ("　" . "　"))
                ("z," nil ("，" . "，"))
                ("zz" nil ("．" . "．"))
                ;; 全角数字
                ("z0" nil ("０" . "０"))
                ("z1" nil ("１" . "１"))
                ("z2" nil ("２" . "２"))
                ("z3" nil ("３" . "３"))
                ("z4" nil ("４" . "４"))
                ("z5" nil ("５" . "５"))
                ("z6" nil ("６" . "６"))
                ("z7" nil ("７" . "７"))
                ("z8" nil ("８" . "８"))
                ("z9" nil ("９" . "９"))
                ;; 記号全角化
                ("!" nil ("！" . "！"))
                ;; 記号半角化
                (";" nil (";" . ";"))
                (":" nil (":" . ":"))
                ;; 括弧
                ("(" nil ("（" . "（"))
                (")" nil ("）" . "）"))
                )))

