;; This file is NOT part of GNU Emacs.

(require 'cl)				; common lisp goodies, loop

(add-to-list 'load-path "~/.emacs.d/lisp")
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil t)
  (url-retrieve
   "https://github.com/dimitri/el-get/raw/master/el-get-install.el"
   (lambda (s)
     (end-of-buffer)
     (eval-print-last-sexp))))

;; now either el-get is `require'd already, or have been `load'ed by the
;; el-get installer.


;; set local recipes
(setq
 el-get-sources
 '(
   (
    :name migemo
          :type github
          :pkgname "emacs-jp/migemo"
          :compile "migemo.el"
          )
   (:name auto-complete
          :type github
          :pkgname "auto-complete/auto-complete"
          :features auto-complete
          :compile "auto-complete.el"
          :depends (popup fuzzy)
   	  )
   (:name fuzzy ;; for auto-complete
          :type github
          :pkgname "auto-complete/fuzzy-el"
          :compile "fuzzy.el"
          )
   (:name popup ;; for auto-complete
          :type github
          :pkgname "auto-complete/popup-el"
          :compile "popup.el"
          )
   (:name shell-switcher
          :type github
          :pkgname "DamienCassou/shell-switcher"
          :features shell-switcher
          :compile "shell-switcher.el"
          :description "An emacs mode to easily switch between shell buffers (like with alt+tab)")
   (:name undo-tree
          :type http
          :url "http://www.dr-qubit.org/download.php?file=undo-tree/undo-tree.el"
          :localname "undo-tree.el"
          :features undo-tree
          :post-init (global-undo-tree-mode)
          )
   (:name popwin
          :type github
          :description "Popup Window Manager."
          :pkgname "m2ym/popwin-el"
          :website "https://github.com/m2ym/popwin-el"
          :features popwin
          )
   (:name ssh-agent
          :type http
          :url "http://glozer.net/code/ssh-agent.el"
          :compile "ssh-agent.el"
          :description "ssh-agent.el runs ssh-agent from within emacs, setting the proper env variables. Also execute ssh-add and prompt the user for any requested passphrases."
          )
   (:name web-mode
          :description "emacs major mode for editing PHP/JSP/ASP HTML templates (with embedded CSS and JS blocks)"
          :type github
          :pkgname "fxbois/web-mode")
   (:name atom-one-dark-theme
          :description "This is a port of the Atom One Dark theme from Atom.io."
          :type github
          :pkgname "jonathanchu/atom-one-dark-theme")

   ;; 何か動かん
   ;; (:name dired-dd
   ;;        :website "http://www.asahi-net.or.jp/~pi9s-nnb/dired-dd-home.html"
   ;;        :type http-tar
   ;;        :options ("xvf")
   ;;        :url "http://www.asahi-net.or.jp/~pi9s-nnb/dired-dd.tar.gz"
   ;;        :description "A drag-and-drop enhancement to Dired"
   ;;        :load-path ("~/.emacs.d/el-get/dired-dd")
   ;;        :build `(,(concat "make emacs=" el-get-emacs " compile"))
   ;;        :after (progn
   ;;                 (add-hook
   ;;                  'dired-load-hook
   ;;                  (function
   ;;                   (lambda ()
   ;;                     (load "dired-x")
   ;;                     ;; Set dired-x variables here.
   ;;                     ;; To and flo...
   ;;                     (if window-system (require 'dired-dd)))))
   ;;                 )
   ;;        )
   ))

;; now set our own packages
(setq
 my:el-get-packages
 '(
   ;; basic environment
   el-get                    ; el-get is self-hosting
   magit                     ; It's Magit! An Emacs mode for Git.
   git-gutter+               ; View, stage and revert Git changes straight from the buffer.
   flymake-cursor            ; displays flymake error msg in minibuffer after delay (illusori/github)
   escreen                   ; Emacs window session manager

   ;; text editing
   ; grep-edit does not work on v24.3
   wgrep
   multiple-cursors  ; An experiment in adding multiple cursors to emacs
   expand-region     ; Expand region increases the selected region by semantic units. Just keep pressing the key until it selects what you want.

   ;; language mode
;   eldoc-extension          ; Some extension for eldoc
   yaml-mode                ; Simple major mode to edit YAML file for emacs
   css-eldoc                ; eldoc plugin for CSS
   js2-mode                 ; javascript mode
   json-mode                ; Major mode for editing JSON files, extends the builtin js-mode to add better syntax highlighting for JSON.
   tern                     ; A JavaScript code analyzer for deep, cross-editor language support.
   ;tern-auto-complete
   coffee-mode              ; coffeescript mode
   php-mode-improved        ; if you're into php...
   zencoding-mode           ; http://www.emacswiki.org/emacs/ZenCoding
   rvm                      ; Emacs integration for rvm
   markdown-mode            ; Major mode to edit Markdown files in Emacs
   dockerfile-mode          ; An emacs mode for handling Dockerfiles.
   python-mode              ; Major mode for editing Python programs
   jedi                     ; An awesome Python auto-completion for Emacs
   sass-mode                ; Major mode for editing Sass files

   ;; color-theme
   color-theme                          ; nice looking emacs

   ;; dired
   dired+                               ; Extensions to Dired
   dired-details+                       ; Make file details hide-able in dired

   ;; tools
   restclient               ; HTTP REST client tool for emacs
   ))

(setq
 my:after-el-get-packages
 '(
   ;; helm
   helm          ; Emacs incremental completion and narrowing framework
   ac-helm       ; Helm interface for auto-complete
   helm-swoop    ; Efficiently hopping squeezed lines powered by Emacs helm interface
   ace-isearch   ; ace-isearch.el provides a minor mode which combines isearch and ace-jump-mode.
   helm-ls-git   ; Yet another helm to list git file.
   helm-ag       ; The silver search with helm interface.
   helm-etags-plus ; etags plus for helm-mode
   ))

;;
;; Some recipes require extra tools to be installed
;;
;; Note: el-get-install requires git, so we know we have at least that.
;;
;; (when (el-get-executable-find "cvs")
;;   (add-to-list 'my:el-get-packages 'PACKAGENAME)) ; the debian addons for emacs

(when (el-get-executable-find "svn")
  (loop for p in '(
		   ;; psvn    		; M-x svn-status
		   yasnippet		; powerful snippet mode
		   )
	do (add-to-list 'my:el-get-packages p)))

(setq my:el-get-packages
      (append
       my:el-get-packages
       (loop for src in el-get-sources collect (el-get-source-name src))
       my:after-el-get-packages))


;; set init files directory
(setq el-get-user-package-directory "~/.emacs.d/el-get-init-files/")

;; install new packages and init already installed packages
(el-get 'sync my:el-get-packages)


;; load setting file
(require 'settings)

;; load ddskk setting file
(require 'ddskk-settings)
